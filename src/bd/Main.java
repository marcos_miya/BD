package bd;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	//java.sql.Date.valueOf("2018-04-18");
	public static Cliente menu1() {
		Scanner scanner = new Scanner(System.in);
		Cliente cliente = new Cliente();
		System.out.println("Insira o nome: ");
		cliente.setNome(scanner.nextLine());
		System.out.println("Insira o CPF: ");
		cliente.setCpf(scanner.nextLine());
		System.out.println("Insira a idade: ");
		cliente.setIdade(Integer.parseInt(scanner.nextLine()));
		System.out.println("Insira a rua: ");
		cliente.setRua(scanner.nextLine());
		System.out.println("Insira o numero: ");
		cliente.setNumero(Integer.parseInt(scanner.nextLine()));
		System.out.println("Insira o complemento: ");
		cliente.setComplemento(scanner.nextLine());
		scanner.close();
		return cliente;
	}
	

	public static void menu2(Cliente cliente) {
		System.out.println("Nome: " + cliente.getNome());
		System.out.println("CPF: " + cliente.getCpf());
		System.out.println("Idade: " + cliente.getIdade());
		System.out.println("Rua: " + cliente.getRua());
		System.out.println("Numero: " + cliente.getNumero());
		System.out.println("Complemento: " + cliente.getComplemento());
	}
	
	public static Cliente menu3(String cpf) {
		Scanner scanner = new Scanner(System.in);
		Cliente cliente = new Cliente();
		System.out.println("Atualizacao de dados do cpf: " + cpf);
		System.out.println("Insira o nome: ");
		cliente.setNome(scanner.nextLine());
		cliente.setCpf(cpf);
		System.out.println("Insira a idade: ");
		cliente.setIdade(Integer.parseInt(scanner.nextLine()));
		System.out.println("Insira a rua: ");
		cliente.setRua(scanner.nextLine());
		System.out.println("Insira o numero: ");
		cliente.setNumero(Integer.parseInt(scanner.nextLine()));
		System.out.println("Insira o complemento: ");
		cliente.setComplemento(scanner.nextLine());
		scanner.close();
		return cliente;
	}
	
	public static void main(String[] args) throws SQLException{
		//Connection conexao = Conexao.getConnection();
		//System.out.println("Sucesso!");
		Scanner scanner = new Scanner(System.in);
		ClienteDao dao = new ClienteDao();
		
		//java.sql.Date.valueOf("2018-04-18");
		//java.sql.Time.valueOf("16:00:00");
		
		
		String fim;
		System.out.println("Pressione 1 para incluir; 2 para consultar todos; 3 para excluir;" +
				" 4 para atualizar; 5 para consultar os dados de um cpf:");
		fim = scanner.nextLine();
		
		//while(!fim.equals("0")) {
		if(fim.equals("1")) {
			Cliente cliente = menu1();
			dao.inserir(cliente);
			
		//}
		}
		else if(fim.equals("2")){
			System.out.println("Digite o CPF: ");
			List<Cliente> clientes = new ArrayList<Cliente>();
			String cpf = scanner.nextLine();
			clientes = dao.consultarCpf(cpf);
			for(Cliente cliente: clientes) {
				menu2(cliente);
			}
		}
			else if(fim.equals("3")){
				System.out.println("Digite o CPF: ");
				List<Cliente> clientes = new ArrayList<Cliente>();
				String cpf = scanner.nextLine();
				clientes = dao.consultarCpf(cpf);
				for(Cliente cliente: clientes) {
					menu2(cliente);
				}
				System.out.println("Deseja excluir mesmo este cliente? (S/N)");
				String ok = scanner.nextLine();
				if(ok.equals("S") || ok.equals("s")) {
					dao.excluir(cpf);
				}
		}else if(fim.equals("4")){
			System.out.println("Digite o CPF: ");
			List<Cliente> clientes = new ArrayList<Cliente>();
			String cpf = scanner.nextLine();
			clientes = dao.consultarCpf(cpf);
			for(Cliente cliente: clientes) {
				menu2(cliente);
			}
			dao.atualizar(menu3(cpf));
			System.out.println("Exclusao efetuada com sucesso!");
		}
		else if(fim.equals("5")){
			System.out.println("Digite o cpf:");
			List<Cliente> clientes = new ArrayList<Cliente>();
			clientes = dao.consultarCpf(scanner.nextLine());
			for(Cliente cliente: clientes) {
				menu2(cliente);
			}
		}
		scanner.close();
		
		//conexao.close();
	}

}
