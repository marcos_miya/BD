package bd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDao {
	private Connection conexao;
	
	public ClienteDao(){
		this.conexao = Conexao.getConnection();
	}
	
	public void inserir(Cliente cliente) {
		String query = "insert into cliente(cpf, nome, idade, rua, numero, complemento)"
				+ " values(?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			statement.setString(1, cliente.getCpf());
			statement.setString(2, cliente.getNome());
			statement.setInt(3, cliente.getIdade());
			statement.setString(4, cliente.getRua());
			statement.setInt(5, cliente.getNumero());
			statement.setString(6, cliente.getComplemento());
			
			statement.execute();
			statement.close();
		}
		catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public List<Cliente> consultar(){
		String query = "select * from cliente";
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			ResultSet resultado = statement.executeQuery();
			int i = 0;
			while(resultado.next()) {
				Cliente cliente = new Cliente();
				cliente.setCpf(resultado.getString("cpf"));
				cliente.setNome(resultado.getString("nome"));
				cliente.setIdade(resultado.getInt("idade"));
				cliente.setRua(resultado.getString("rua"));
				cliente.setNumero(resultado.getInt("numero"));
				cliente.setComplemento(resultado.getString("complemento"));
				clientes.add(cliente);
			}
			
			resultado.close();
			statement.close();
			
			return clientes;
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public List<Cliente> consultarCpf(String cpf){
		String query = "select * from cliente where cpf = (?)";
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			statement.setString(1, cpf);
			
			ResultSet resultado = statement.executeQuery();
			
			while(resultado.next()) {
				Cliente cliente = new Cliente();
				cliente.setCpf(resultado.getString("cpf"));
				cliente.setNome(resultado.getString("nome"));
				cliente.setIdade(resultado.getInt("idade"));
				cliente.setRua(resultado.getString("rua"));
				cliente.setNumero(resultado.getInt("numero"));
				cliente.setComplemento(resultado.getString("complemento"));
				clientes.add(cliente);
			}
			
			resultado.close();
			statement.close();
			
			return clientes;
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void atualizar(Cliente cliente) {
		String query = "update cliente"
				+ " set nome = (?), idade = (?), rua = (?), numero = (?), complemento =(?) where cpf = (?)";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			statement.setString(6, cliente.getCpf());
			statement.setString(1, cliente.getNome());
			statement.setInt(2, cliente.getIdade());
			statement.setString(3, cliente.getRua());
			statement.setInt(4, cliente.getNumero());
			statement.setString(5, cliente.getComplemento());
			
			statement.execute();
			statement.close();
		}
		catch(SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public void excluir(String cpf) {
		String query = "delete from cliente where cpf = (?)";
				
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			statement.setString(1, cpf);
			
			statement.execute();
			
			statement.close();
			
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
